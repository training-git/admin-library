<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryTable extends Migration
{
    
    public function up()
    {
        Schema::create('student', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
        });

        Schema::create('book', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
        });

        Schema::create('rent', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('book_id', 255);
            $table->integer('student_id', 255);
            $table->timestamps('date');
            $table->integer('duration', 255);
        });
    }

    public function down()
    {
        Schema::dropIfExists('student');
        Schema::dropIfExists('book');
        Schema::dropIfExists('rent');
    }
}
