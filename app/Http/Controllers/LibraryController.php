<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;

class LibraryController extends Controller
{

    public function index()
    {
        $rent = DB::table('rent')
                ->join('book', 'rent.book_id', '=', 'book.id') 
                ->join('student', 'rent.student_id', '=', 'student.id')    
                ->select('rent.id as id', 'rent.date as date',   'book.name as b_name', 'student.name as s_name', 'rent.duration as duration')
                ->get();
        return view('index',['rent' => $rent]);
    }

    public function create()
    {
        $book = DB::table('book')->get();
        $student = DB::table('student')->get();
        return view('add',['book' => $book, 'student' => $student]);
    }

    public function store(Request $request)
    {
        $date = new DateTime();
        DB::table('rent')->insert([
            'book_id' => $request->book,
            'student_id' => $request->student,  
            'date' => $date,
            'duration' => $request->duration
        ]);
        return redirect('/rent/list');
    }

    public function destroy($id)
    {
        DB::table('rent')->where('id', $id)->delete();
        return redirect('/rent/list');
    }
}
