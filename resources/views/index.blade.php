<html>
<head>
<link rel="stylesheet" href="/css/bootstrap.css">
<link rel="stylesheet" href="/css/style.css">
	<title></title>
</head>
<body>

	<h3>Rent Data</h3>
	<table class="table table-dark">
		<tr>
			<th>Book Name</th>
			<th>Student Name</th>
            <th>Date</th>
			<th>Duration</th>
		</tr>
		@foreach($rent as $r)
		<tr>
			<td>{{ $r->b_name }}</td>
			<td>{{ $r->s_name }}</td>
            <td>{{ $r->date   }}</td>
			<td>{{ $r->duration }}</td>
            <td><a href="/rent/remove/{{  $r->id  }}" onclick="return confirm('Are you sure?')">Done</a></td>
		</tr>
		@endforeach
    </table>
    <a href="/rent/add" class="button1">Add Data</a>
 
 
</body>
</html>