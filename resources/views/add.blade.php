<!DOCTYPE html>
<html>
<link rel="stylesheet" href="/css/bootstrap.css">
<link rel="stylesheet" href="/css/style.css">
<head>
	<title></title>
</head>
<body>
    <a href="/rent/list">Back</a>
    
	<h3 class="header">Rent Now!</h3>
    
    <div class="form-container">
        <form action="/rent/store" method="post">
        @csrf
            <select name="book" class="form-control" id="select">
            @foreach($book as $b)
            <option value="{{$b->id}}" name="opt1">{{$b->name}}</option>
            @endForeach 
            </select>
            <select name="student" class="form-control" id="select">
            @foreach($student as $s)
            <option value="{{$s->id}}" name="opt2">{{$s->name}}</option>
            @endForeach 
            </select>
            Duration <input type="number" name="duration" required="required"> <br/>
            <button type="submit" class="btn btn-primary" id="rentbutton">Rent</button>
        </form>
    </div>

</body>
</html>